package com.techuniversity;

import java.io.InputStream;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        System.out.println( "Hello World!" );
        App app = new App();
        Properties properties;
        properties = app.loadPropertiesFile("db.properties");
        properties.forEach((k, v) ->  System.out.println( k + " " + v + " "));
    }
    public Properties loadPropertiesFile(String filename) {
        Properties properties = new Properties();
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filename);
            properties.load(is);

        }catch (Exception e) {
            System.out.println( "No se puede acceder a los recursos hotfix" );
        }

        return properties;
    }
}
